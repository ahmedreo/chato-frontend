const webpack = require('webpack');

const env = require('dotenv').config();

module.exports = {
    entry: __dirname + '/bootstrap/app.js',
    output: {
        path: __dirname + '/public/assets/js' ,
        filename: "bundle.min.js"
    },
    plugins : [
        new webpack.DefinePlugin({
            'apiHost': JSON.stringify(process.env.SERVER_URL),
        }),
        new webpack.EnvironmentPlugin([
            'APP_NAME', 
            'SERVER_URL' , 
            'SOCKET_URL' , 
            'REDIS_HOST' , 
            'REDIS_PORT' , 
            'REDIS_PASSWORD'
        ]),
        // new webpack.optimize.UglifyJsPlugin({
        //     mangle:false,
        //     minimize: true,
        //     output: {
        //         comments: false
        //     },
        //     compress: {
        //         warnings: false,
        //         screw_ie8: true
        //     }
        // }),
    ],
};