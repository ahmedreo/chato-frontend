app.service("RoomEvent", function($rootScope , RoomService , authService , flashService){
	$rootScope.$on('Companionship-success', function(event) {
		flashService.flash('success' , 'Well done! you have successfully invite him.')
	});

	$rootScope.$on('deleteFriend-success', function(event) {
		flashService.flash('success' , 'Well done! you have successfully deleted him')
	});
	$rootScope.$on('addCloseFriend-success', function(event) {
		flashService.flash('success' , 'Well done! you have successfully added him to close list.')
	});

	$rootScope.$on('deleteCloseFriend-success', function(event) {
		flashService.flash('success' , 'Well done! you have successfully deleted from close list')
	});
});