app.service("RoomService", function( $rootScope , $http, $cookies, $window , PermRoleStore , authService ) {

    var self = this;
    
    this.sendInvitation = function(id) {
        return $http.post( apiHost + 'invitation' , {recipient_id : id})
    };
    
    this.acceptInvitation = function(id) {
        return $http.post( apiHost + 'acceptInvitation' , {invitation_id : id})
    };
    this.declineInvitation = function(id) {
        return $http.delete( apiHost + 'invitation/' + id)
    };
    this.deleteFriend = function(id) {
        return $http.delete( apiHost + 'relation/' + id)
    };
    this.addCloseFriend = function(id) {
        return $http.post( apiHost + 'addCloseFriend' , {relation_id : id})                
    };
    this.deleteCloseFriend = function(id) {
        return $http.post( apiHost + 'deleteCloseFriend' , {relation_id : id})
    };
    this.searchFriends = function(query) {
        return $http.post( apiHost + 'search' , {query : query , attribute : 'username' , model :'user'})
    };
    this.getMessages =function(recipient_id , messages_ids){
        return $http.post(apiHost + 'getMessages' , {recipient_id : recipient_id , messages_ids : messages_ids});
    }
    this.sendMessage =function(message , recipient_id){
        return $http.post(apiHost + 'message' , {body : message , recipient_id : recipient_id},{ignoreLoadingBar: true});
    }
    this.readMessages = function (ids) {
        return $http.post(apiHost + 'readMessages' , {messages_ids : ids},{ignoreLoadingBar: true});
    }

    
});