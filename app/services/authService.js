app.service("authService", function( $rootScope , $http , $localStorage , $auth ) {

  var self = this;

  this.pushModel = function(name , model){
    $localStorage.user[name].push(model);
  }

  this.updateField = function(name ,field, model){
    _.forEach($localStorage.user[name] , function(k){
        if(k[field]._id === model._id){
          k[field].active = model.active;  
        }
    });
  }

  this.getAuth = function (){
    if(!self.loginSession){
      return $http.post(apiHost + 'authenticate/user').then(function(r){
        $localStorage.user = r.data.user
      })
    }
  }

  this.logout = function (){
    return $http.post(apiHost + "logout")
  }
});