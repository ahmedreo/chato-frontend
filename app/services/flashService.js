app.service("flashService", function(Flash) {
  
  var self = this;

  self.flash = function( status , message , duration ){
    Flash.create(status, message, duration || 4000 , {class : 'animated slideInDown'});
  }

});