app.controller('LoginCtrl', function ($scope , $rootScope, $http , $localStorage , $route , $location , PermRoleStore , authService , $auth) { 
    $scope.login = function(credentials){

	    $auth.login(credentials).then(function(r) {

	    	$localStorage.user =  r.data.user;
	    	
	    	authService.loginSession =  true;

   			PermRoleStore.defineRole('USER', function () {return true})

	    	$rootScope.$broadcast('login-success');

	    }, function(){

	    	$rootScope.$broadcast('login-failed' );
	    	
	    })
    }

    $scope.authenticate = function(provider) {
      $auth.authenticate(provider);
    }; 
})

