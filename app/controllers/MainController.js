app.controller('MainCtrl', function ($scope , $rootScope, $http , $location , authService , RoomService , $localStorage,flashService , $auth , PermRoleStore , $sessionStorage) {

    $scope.$storage = $localStorage;

    if ($auth.isAuthenticated()) {
        PermRoleStore.defineRole('USER', function () {return true})
        
    }
    else{
        PermRoleStore.defineRole('USER', function () {return false})
    }


    $scope.logout = function(){
        authService.logout().then(function(){
            $auth.logout();
            $localStorage.$reset();
            $sessionStorage.$reset();
            PermRoleStore.defineRole('USER', function () {return false})
            $rootScope.$broadcast('logout-success');
        })
    }

    $scope.acceptInvitation = function(id){
        RoomService.acceptInvitation(id).then(function(r){
          $localStorage.user.relations = r.data.relations;
          $localStorage.user.invitations = r.data.invitations;
      });
    }
    $scope.declineInvitation = function (id) {
        RoomService.declineInvitation(id).then(function(r){
          $localStorage.user.invitations = r.data.invitations;
      })
    }


    if ($localStorage.user) {
        socket.on('invitation-channel:App\\Events\\InvitationCreated#'+ $scope.$storage.user._id , function(data){
            $scope.$apply(function(){
              authService.pushModel('invitations' , data.invitation);
            })
        }) 
    }     
    
    $rootScope.$on('login-success', function(event) {
        socket.on('invitation-channel:App\\Events\\InvitationCreated#'+ $scope.$storage.user._id , function(data){
            $scope.$apply(function(){
              authService.pushModel('invitations' , data.invitation);
            })
        }) 
        $location.path('/app')
        flashService.flash('success' , 'Well done! you have successfully logged in.')
    });
    
    $rootScope.$on('registeration-success', function(event) {
        $location.path('/login');
        flashService.flash('success' , 'Well done! you have successfully created account. please login')
    });

    $rootScope.$on('logout-success', function(event) {
        $location.path('/');
        flashService.flash('success' , 'Well done! you have successfully logged out.')
    });
    $rootScope.$on('login-failed', function(event) {
        flashService.flash('danger' , 'credentials does not match our records ' )
    });

})

