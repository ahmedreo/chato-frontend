app.controller('RegisterCtrl', function ($scope , $http ,$rootScope, $cookies , $location , cfpLoadingBar , $auth , ValidationService) {
    $scope.submitRegistration =function(registrationData){
    	if(new ValidationService().checkFormValidity($scope)) {
    		$auth.signup(registrationData)
    		.then(function(response) {
    			$rootScope.$broadcast('registeration-success');	
    		})
    		.catch(function(response) {

    		});
  		}
    }
})

