app.controller('RoomCtrl', function ($scope , $rootScope, $http , $cookies , $sessionStorage, $localStorage , $location ,$auth, authService , RoomService  , RoomEvent , RoomFactory) { 

  $scope.$storage = $sessionStorage.$default({
    window_ids: []
  });

  $scope.$storage = $localStorage;

  $scope.factory = RoomFactory;  

  $scope.refreshStorage = function(){
    $scope.storageRelations = _.map($sessionStorage.window_ids , function(k){
      return _.find($localStorage.user.relations, ['_id' , k])
    })  
    return true;
  }

  $scope.refreshStorage();
  
  $scope.openWindow = function(id){

    vm = {}

    vm.wrapper = angular.element(document).find('#wrapper');

    vm.widgets = vm.wrapper.children('chat-widget');

    if(! (vm.widgets.length > 2) ){

      if(!_.includes($sessionStorage.window_ids , id)){

        if (id) {$sessionStorage.window_ids.push(id)}

        $scope.refreshStorage();

      }
      else{
        console.log(id)
      }
    }
    else{
      console.log('stop here')
    }
  }

  $scope.closeWindow = function (id) {
    _.pull($sessionStorage.window_ids , id)
    $scope.refreshStorage();
  }

  $scope.sendInvitation = function(id){
    RoomService.sendInvitation(id).then(function (r) {
      $rootScope.$broadcast('Companionship-success');      
    });
  }
  $scope.deleteFriend = function(id , username){
    RoomService.deleteFriend(id).then(function(r){
      $localStorage.user.relations= r.data.relations;
      $rootScope.$broadcast('deleteFriend-success');
    })
  }
  $scope.searchFriends = function(){
    RoomService.searchFriends($scope.Query).then(function (r) {
      RoomFactory.searchResults = r.data.results
    });    
  }

  $scope.addCloseFriend = function(id){
    RoomService.addCloseFriend(id).then(function(r){
      $localStorage.user.relations = r.data.relations;
      $rootScope.$broadcast('addCloseFriend-success');
    })
  }
  $scope.deleteCloseFriend = function(id){
    RoomService.deleteCloseFriend(id).then(function(r){
      $localStorage.user.relations = r.data.relations;
    })
  }

  socket.on('invitation-channel:App\\Events\\InvitationAccepted#'+ $localStorage.user._id , function(data){
    $scope.$apply(function(){
      authService.pushModel('relations' , data.for_sender);
    })
  })

  socket.on('message-channel:App\\Events\\MessageSended#'+ $localStorage.user._id , function(data){
    $scope.$apply(function(){
      $relation = RoomFactory.findRelation(data.message.sender_id)
      if (!_.includes($sessionStorage.window_ids , $relation._id)) {
        $relation.messages.push(data.message)
      }
    })
  })

  socket.on('user-channel:App\\Events\\UserStateChanged#'+ $localStorage.user._id  , function(data){
      $scope.$apply(function(){
        authService.updateField('relations' , 'friend' , data)
        $scope.refreshStorage();
      })
  })

})


