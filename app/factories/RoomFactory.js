app.factory('RoomFactory', function($localStorage){

	return {
		mergeTwoModel : function (first , seconed) {
			return _.merge(first , seconed)
		},
		unshiftModel : function(name, model){
			$localStorage.user[name].unshift(model)
		},
		findRelation: function (id) {
			return _.find($localStorage.user.relations , {'friend_id' : id}) || false;
		},
		findFriend : function(id) {     		
     		return this.findRelation(id).friend || false;
  		},
  		isFriend : function(id) {
    		return Boolean(this.findFriend(id));
  		},
		getNewMessages : function(messages){
			if (messages) {
				return _.filter(messages , {'seen': false})
			}
		},
		pushModel:function(name , model){
    		$localStorage.user[name].push(model);
  		}				
	}
})