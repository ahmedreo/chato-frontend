app.directive('bootstrapValidationDecorator', function($timeout) {	
    return {
            scope: {
                validationIcon : '&',
                validationState : '&'
            },
            restrict: 'A',
            require:  '^form',
            link: function (scope, el, attrs, formCtrl) {
                var form = formCtrl; 
                var input = angular.element( el[0].querySelector("[name]") )
                var inputA = input.attr('name')
                scope.$watch(function(){return form[inputA].$valid} , function(newVal, oldVal){
                    if(newVal){
                        el.removeClass('has-error').addClass('has-success')
                        el.find('.form-control-feedback').removeClass('glyphicon-remove').addClass('glyphicon-ok')
                    }
                    else{
                        el.removeClass('has-success')
                    }
                })
                input.on('blur' , function(){
                    if(form[inputA].$invalid && form[inputA].$dirty){
                        el.removeClass('has-success').addClass('has-error')
                        el.find('.form-control-feedback').removeClass('glyphicon-ok').addClass('glyphicon-remove')
                    }
                })
            }
        }
});
