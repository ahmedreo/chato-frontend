app.directive('superAnimation', function() {	
    return {
            restrict: 'A',
            scope:{},
            link: function (scope, el ,attr) {
                
                var optionsArray = attr.superAnimation.split("|");

                scope.options = {
                    event : optionsArray[0],
                    effectIn : optionsArray[1],
                    effectOut : optionsArray[2],
                    target : optionsArray[3],
                    node : optionsArray[4],
                    displayStatu : optionsArray[5],
                }

                if(scope.options.node){
                    scope.effectedE = el[scope.options.node](scope.options.target)
                }
                else{
                    scope.effectedE = angular.element(document).find(scope.options.target);
                }

                scope.displayStatu = Boolean(scope.options.displayStatu) || false;   

                scope.init = !scope.displayStatu;

                scope.init ? scope.effectedE.hide() : scope.effectedE.show();

                el.one(scope.options.event , function(){

                    scope.init ? scope.effectedE.show() : null; 

                });


                el.on(scope.options.event ,  function(event) {
                    
                    event.stopPropagation()

                    event.preventDefault();

                    if(scope.init){

                    	scope.effectedE.removeClass(scope.options.effectOut).addClass(scope.options.effectIn);
                	}
                	else{
                		scope.effectedE.removeClass(scope.options.effectIn).addClass(scope.options.effectOut);
                	}

                    scope.init = !scope.init;


                });
            }
        }
});