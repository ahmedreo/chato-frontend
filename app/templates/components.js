app.directive('chatWidget' , function(RoomService , $timeout , $localStorage){
	return {
		restrict: 'E',
		scope: true,
		templateUrl: 'resources/templates/chatWidget.html'	,
		link : function(scope , el){
			
			$timeout(function(){
				if (scope.relation) {
					RoomService.getMessages(scope.relation.friend_id , _.map(scope.relation.messages, '_id')).then(function(r){
						scope.messages_as_sender = r.data.messages_as_sender;
						scope.messages_as_recipient = r.data.messages_as_recipient;
						$localStorage.user.relations = r.data.relations;  
					})						
				}
			}, 300)

			scope.sendMessage = function(message){
				scope.message = null
    			RoomService.sendMessage(message , scope.relation.friend_id).then(function(r){
					scope.messages_as_sender = r.data.messages_as_sender
  				})
			}
			el.find('.send-button').on('click' , function(e){
				if (scope.message) {
					scope.sendMessage(scope.message)
				}
			})

			el.find('.message-box').on('keypress' , function(e){
				if (e.which == 13 || e.keyCode == 13) {
					e.preventDefault()
					if (scope.message) {
						scope.sendMessage(scope.message)
					}
    			}
			})
			socket.on('message-channel:App\\Events\\MessageSended#'+ $localStorage.user._id  , function(data){
				scope.$apply(function(){
					if (scope.relation.friend_id === data.message.sender_id) {
						scope.messages_as_recipient.push(data.message)
					}
				})
			})
		}
	}
})
