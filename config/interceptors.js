export default function interceptors ($httpProvider, $provide ) {

    function redirectWhenLoggedOut($q, $injector) {

        return {
            responseError: function(rejection) {

                var $location = $injector.get('$location'),

                    $localStorage = $injector.get('$localStorage'),
                    
                    $sessionStorage = $injector.get('$sessionStorage'),
                    
                    PermRoleStore = $injector.get('PermRoleStore'),
                    
                    rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid'];

                angular.forEach(rejectionReasons, function(value, key) {

                    if(rejection.data.error === value) {

                        $localStorage.$reset();

                        $sessionStorage.$reset();
                        
                        PermRoleStore.defineRole('USER', function () {return false})

                        $location.path('/login');

                    }

                });

                return $q.reject(rejection);
            }
        }
    }

    $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);

    $httpProvider.interceptors.push('redirectWhenLoggedOut');
}
