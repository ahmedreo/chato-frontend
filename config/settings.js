export default function settings ($locationProvider , FlashProvider , cfpLoadingBarProvider , $translateProvider , $authProvider , $qProvider ) {

    $locationProvider.html5Mode(true);
    $qProvider.errorOnUnhandledRejections(false)
    
    FlashProvider.setTimeout(5000);
    FlashProvider.setShowClose(true);
    FlashProvider.setOnDismiss();
    
    cfpLoadingBarProvider.latencyThreshold = 1;
    // cfpLoadingBarProvider.includeBar = false;

    $translateProvider.preferredLanguage('en');

    $authProvider.loginUrl = apiHost +'authenticate';
    $authProvider.signupUrl = apiHost + 'register';
    $authProvider.facebook({
      clientId: '1512384118780103',
      url: apiHost + 'auth/facebook',
    });

}
