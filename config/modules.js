require('angular')
require('angular-animate')
require('angular-route')
require('angular-moment')
require('angular-permission')
require('angular-loading-bar')
require('angular-flash-alert')
require('angular-validation-ghiscoding')
require('angular-cookies')
require('angular-translate')
require('satellizer')
require('ngstorage')

modules.providers = [
    'ngRoute' ,
    'satellizer',
    'ngCookies' ,
    'angular-loading-bar' ,
    'cfp.loadingBar' ,
    'permission',
    'permission.ng' ,
  	'ngFlash',
   	'ghiscoding.validation', 
   	'pascalprecht.translate',
    'ngAnimate',
    'angularMoment',
  	'ngStorage'
]


export default function modules(){};





