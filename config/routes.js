export default function routes($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "resources/views/WelcomeView.html",
        controller: 'WelcomeCtrl',
    })
    .when("/login", {
        templateUrl : "resources/views/LoginView.html",
        controller: 'LoginCtrl',
        data: {
          permissions: {
            except: ['USER'],
             redirectTo: '/'
          }
        }
    })
    .when("/register", {
        templateUrl : "resources/views/RegistrationView.html",
        controller: 'RegisterCtrl',
        data: {
          permissions: {
            except: ['USER'],
             redirectTo: '/'
          }
        }
    })
    .when('/profile/:id', {
        templateUrl : "resources/views/ProfileView.html",
        controller: 'ProfileCtrl' ,  
        data: {
          permissions: {
            only: ['USER'],
             redirectTo: '/login'
          }
        }
    })       
    .when('/app', {
        templateUrl : "resources/views/RoomView.html",
        controller: 'RoomCtrl' ,  
        resolve: {
          Auth: function(authService){
            return authService.getAuth();
          }
        },
        data: {
          permissions: {
            only: ['USER'],
             redirectTo: '/login'
          }
        }
    })            
    .otherwise({
        templateUrl : "resources/views/404.html",
    })
}