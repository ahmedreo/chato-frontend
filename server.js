var env = require('dotenv').config();

var cors = require('cors')

var express = require('express');

var app = express(); 

var server = require('http').Server(app);

var io = require('socket.io')(server);

var Redis = require('ioredis');

var redis = new Redis({
	port: process.env.REDIS_PORT ,         
	host: process.env.REDIS_HOST,   
	password: process.env.REDIS_PASSWORD
})

redis.subscribe('message-channel');

redis.subscribe('invitation-channel');

redis.subscribe('user-channel');

redis.on('message' , function (channel , message) {
	
	message = JSON.parse(message);

	if(message.event === 'App\\Events\\InvitationCreated'){
		console.log(message)
		io.emit(channel + ':' + message.event + "#" + message.data.invitation.recipient_id , message.data);	
	}

	else if(message.event === 'App\\Events\\InvitationAccepted'){
		io.emit(channel + ':' + message.event + "#" + message.data.for_sender.user_id , message.data);	
	}

	else if(message.event === 'App\\Events\\UserStateChanged'){
		var auth_state = message.data.auth_state,
			friend_ids = message.data.friend_ids
		for (var i = friend_ids.length - 1; i >= 0; i--) {
			io.emit(channel + ':' + message.event +"#"+ friend_ids[i] , auth_state )
		}
	}
	
	else if(message.event === 'App\\Events\\MessageSended'){
			io.emit(channel + ':' + message.event +"#"+ message.data.recipient_id , message.data )

	}
})


io.on('connection', function (socket) {
	console.log(1)
});

app.set('port', process.env.PORT || 3000);


server.listen(app.get('port'));

app.use(express.static(__dirname + '/public'))
app.use('/node_modules' , express.static(__dirname + '/node_modules'))


app.all('/*' , cors(),function(request , response){
	response.sendFile(__dirname + '/public/index.html')

})
