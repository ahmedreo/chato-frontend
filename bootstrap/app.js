import modules from '../config/modules';

import routes from '../config/routes';

import interceptors from '../config/interceptors';

import settings from '../config/settings';

window.app = angular.module(process.env.APP_NAME, modules.providers)

window.socket = require('socket.io-client')(process.env.SOCKET_URL);

app.config(routes)

app.config(interceptors)

app.config(settings)


require('./vendors.js')
require('../app/factories/')
require('../app/services/')
require('../app/events/')
require('../app/templates/components.js')
require('../app/directives/')
require('../app/controllers/')
